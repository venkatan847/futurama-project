import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:futurama/model/characters_model.dart';

class CharacterProvider extends ChangeNotifier {
  bool isLoading = true;
  CharactersData? selectedData;

  CharacterProvider() {
    getCharacter();
  }

  List<CharactersData> characters = [];

  void getCharacter() async {
    try {
      var response =
          await Dio().get('https://api.sampleapis.com/futurama/characters');
      for (var item in response.data) {
        characters.add(CharactersData.fromJson(item));
      }
      isLoading = false;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
