import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:futurama/model/home_model.dart';

class HomeProvider extends ChangeNotifier {
  bool isLoading = true;

  HomeProvider() {
    getInfoData();
  }

  List<HomeData> home = [];

  void getInfoData() async {
    try {
      var response =
          await Dio().get('https://api.sampleapis.com/futurama/info');
      for (var item in response.data) {
        home.add(HomeData.fromJson(item));
      }
      isLoading = false;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
