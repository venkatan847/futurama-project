import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:futurama/model/quiz_model.dart';

class QuizProvider extends ChangeNotifier {
  bool isLoading = true;
  int currentIndex = 0;
  int score = 0;
  String? selectedAnswer = "";

  QuizProvider() {
    getQuizQuestions();
  }

  List<QuizData> quizData = [];
  List<QuestionSelectedModel> selectedquizData = [];

  void getQuizQuestions() async {
    try {
      var response =
          await Dio().get('https://api.sampleapis.com/futurama/questions');
      for (var item in response.data) {
        quizData.add(QuizData.fromJson(item));
      }
      isLoading = false;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  resetQuiz() {
    score = 0;
    currentIndex = 0;
    selectedquizData.clear();
    notifyListeners();
  }

  setAnswer(QuizData? data, String? selectQuestion) {
    selectedAnswer = selectQuestion;
    notifyListeners();
  }

  updateQuizResult(QuizData? data, String? answer) {
    final question = selectedquizData
        .where((element) => element.question?.id == data?.id)
        .toList();
    if (question.length == 0) {
      selectedquizData
          .add(QuestionSelectedModel(question: data, selectedAnswer: answer));
    }
  }

  submitQuestion(QuizData? data, String? answer) {
    if (currentIndex != quizData.length) {
      currentIndex++;
      if (data?.correctAnswer == answer) {
        score++;
        selectedAnswer = "";
      }
      selectedAnswer = "";
      updateQuizResult(data, answer);
      notifyListeners();
    }
  }
}

class QuestionSelectedModel {
  QuestionSelectedModel({this.question, this.selectedAnswer});

  QuizData? question;
  String? selectedAnswer;
}
