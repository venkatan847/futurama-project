import 'package:flutter/material.dart';
import 'package:futurama/appTheam/appstate_notifier.dart';
import 'package:futurama/appTheam/apptheme.dart';
import 'package:futurama/characters/characters_details.dart';
import 'package:futurama/home/home_screen.dart';
import 'package:futurama/providers/characters_provider.dart';
import 'package:futurama/providers/home_provider.dart';
import 'package:futurama/providers/quiz_provider.dart';
import 'package:futurama/quiz/results_page.dart';

import 'package:provider/provider.dart';

import 'characters/characters_screen.dart';
import 'quiz/quiz_screen.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
          create: (context) => AppStateNotifier(isDarkModeOn: false)),
      ChangeNotifierProvider<HomeProvider>(create: (_) => HomeProvider()),
      ChangeNotifierProvider<CharacterProvider>(
          create: (_) => CharacterProvider()),
      ChangeNotifierProvider<QuizProvider>(create: (_) => QuizProvider())
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateNotifier>(builder: (context, appState, child) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: AppTheme.lightTheme,
        darkTheme: AppTheme.darkTheme,
        themeMode: appState.isDarkModeOn ? ThemeMode.dark : ThemeMode.light,
        initialRoute: "/home",
        routes: {
          "/home": (BuildContext context) => HomeScreen(),
          "/character": (BuildContext context) => CharactersScreen(),
          "/quiz": (BuildContext context) => QuizScreen(),
          "/characterDetail": (BuildContext context) => CharactersDetails(),
          "/results": (BuildContext context) => QuizResultDetails()
        },
        home: HomeScreen(),
      );
    });
  }
}
//HomeScreen(), CharactersScreen(), QuizScreen()