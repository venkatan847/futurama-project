import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/characters_provider.dart';

class CharactersDetails extends StatelessWidget {
  CharactersDetails({Key? key}) : super(key: key);
  CharacterProvider? provider;

  @override
  Widget build(BuildContext context) {
    provider = Provider.of<CharacterProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Wrap(
          children: [
            Text((provider?.selectedData!.name?.first ?? "") + " "),
            Text((provider?.selectedData!.name?.middle ?? "") + " "),
            Text((provider?.selectedData!.name?.last ?? "")),
            Text('\'s Biography'),
          ],
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body:
          ListView(children: [characterImage(), characterInfo(), sayinginfo()]),
    );
  }

  Widget characterImage() {
    return Container(
        height: 150,
        child: Image.network(provider?.selectedData!.images?.main ?? ""));
  }

  Widget characterInfo() {
    return Container(
      child: Column(children: [
        inofData("Age:", provider?.selectedData?.age ?? ""),
        inofData("Gender", provider?.selectedData?.gender ?? ""),
        inofData("Species", provider?.selectedData?.species ?? ""),
        inofData("homePlant", provider?.selectedData?.homePlanet ?? ""),
        inofData("occupation", provider?.selectedData?.occupation ?? "")
      ]),
    );
  }

  Widget inofData(String title, String value) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
          Container(
            child:
                Text(value, style: TextStyle(fontSize: 16, color: Colors.blue)),
            width: 200,
          )
        ],
      ),
    );
  }

  Widget sayinginfo() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Container(
        height: 300,
        child: ListView.builder(
            itemCount: provider?.selectedData?.sayings?.length,
            itemBuilder: (BuildContext context, index) {
              return Padding(
                  padding: EdgeInsets.only(left: 6, top: 8),
                  child: Text(provider!.selectedData!.sayings![index]));
            }),
      ),
    );
  }
}
