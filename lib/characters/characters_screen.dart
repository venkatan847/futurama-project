import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/characters_provider.dart';

class CharactersScreen extends StatelessWidget {
  const CharactersScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CharacterProvider provider = Provider.of<CharacterProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Characters'),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: provider.isLoading
          ? Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            )
          : ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(height: 1);
              },
              itemBuilder: (context, index) {
                final characterData = provider.characters[index];
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: (() {
                      provider.selectedData = characterData;
                      Navigator.pushNamed(context, "/characterDetail");
                    }),
                    child: ListTile(
                        leading:
                            Image.network(characterData.images?.main ?? ""),
                        title: Row(
                          children: [
                            Text((characterData.name?.first ?? "") + " "),
                            Text((characterData.name?.middle ?? "") + " "),
                            Text(characterData.name?.last ?? "")
                          ],
                        )),
                  ),
                );
              },
              itemCount: provider.characters.length,
            ),
    );
  }
}
