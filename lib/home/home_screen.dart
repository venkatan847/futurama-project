import 'package:flutter/material.dart';
import 'package:futurama/providers/home_provider.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HomeProvider provider = Provider.of<HomeProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: provider.isLoading
          ? Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
              ),
            )
          : ListView.builder(
              itemBuilder: (context, index) {
                dynamic homeData;
                if (index != 0) {
                  homeData = provider.home.first.creators![index - 1];
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 10.0, vertical: 15.0),
                  child: index == 0
                      ? Container(
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Center(
                                  child: Text(
                                    'Futurama Show Info',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.teal),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(provider.home.first.synopsis!),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                      "YearsAired: ${provider.home.first.yearsAired}",
                                      style: TextStyle(fontSize: 16)),
                                ),
                              ]),
                        )
                      : Column(children: [
                          if (index == 1)
                            Padding(
                                padding: EdgeInsets.only(
                                  bottom: 10,
                                ),
                                child: Text(
                                  'Creators',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.teal),
                                )),
                          ListTile(
                              //leading: Image.network(homeData.url ?? ""),
                              title: Column(
                            children: [
                              Row(
                                children: [
                                  Text("Name:  "),
                                  Text(homeData.name ?? "",
                                      style: TextStyle(color: Colors.blue))
                                ],
                              ),
                              Row(
                                children: [
                                  Text("Website Link:  "),
                                  Container(
                                      width: 200,
                                      height: 50,
                                      child: Text(
                                        homeData.url ?? "",
                                        style: TextStyle(color: Colors.blue),
                                      ))
                                ],
                              ),
                            ],
                          )),
                          if (index == 2)
                            Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                      color: Colors.blueAccent,
                                      child: TextButton(
                                        child: Text(
                                          'Characters',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                              context, "/character");
                                        },
                                      ),
                                    ),
                                    Container(
                                      width: 100,
                                      color: Colors.blueAccent,
                                      child: TextButton(
                                        child: Text(
                                          'Quiz',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pushNamed(context, "/quiz");
                                        },
                                      ),
                                    )
                                  ],
                                ))
                        ]),
                );
              },
              itemCount: provider.home.first.creators!.length + 1,
            ),
    );
  }
}
