class HomeData {
  String? synopsis;
  String? yearsAired;
  List<Creators>? creators;
  int? id;

  HomeData({this.synopsis, this.yearsAired, this.creators, this.id});

  HomeData.fromJson(Map<String, dynamic> json) {
    synopsis = json['synopsis'];
    yearsAired = json['yearsAired'];
    if (json['creators'] != null) {
      creators = <Creators>[];
      json['creators'].forEach((v) {
        creators!.add(new Creators.fromJson(v));
      });
    }
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['synopsis'] = this.synopsis;
    data['yearsAired'] = this.yearsAired;
    if (this.creators != null) {
      data['creators'] = this.creators!.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    return data;
  }
}

class Creators {
  String? name;
  String? url;

  Creators({this.name, this.url});

  Creators.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['url'] = this.url;
    return data;
  }
}
