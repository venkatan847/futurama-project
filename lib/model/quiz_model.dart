class QuizData {
  int? id;
  String? question;
  List<String>? possibleAnswers;
  String? correctAnswer;
  QuizData({this.id, this.question, this.possibleAnswers, this.correctAnswer});

  QuizData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    question = json['question'];
    possibleAnswers = json['possibleAnswers'].cast<String>();
    correctAnswer = json['correctAnswer'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['question'] = this.question;
    data['possibleAnswers'] = this.possibleAnswers;
    data['correctAnswer'] = this.correctAnswer;
    return data;
  }
}
