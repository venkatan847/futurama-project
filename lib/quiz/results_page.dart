import 'package:flutter/material.dart';
import 'package:futurama/providers/quiz_provider.dart';
import 'package:provider/provider.dart';

class QuizResultDetails extends StatelessWidget {
  QuizResultDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    QuizProvider provider = Provider.of<QuizProvider>(context);
    return Scaffold(
        appBar: AppBar(
          title: Text("Results"),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: ListView(children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.8,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: provider.selectedquizData.length + 1,
              itemBuilder: (context, index) {
                dynamic data;
                if (index != 0) {
                  data = provider.selectedquizData[index - 1];
                }
                return index == 0
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "Actual Answers",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text("Yout Answers",
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold))
                          ],
                        ),
                      )
                    : Row(
                        children: [
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width / 2,
                              height: 40,
                              child: Text(data.question?.correctAnswer ?? "")),
                          Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width / 2,
                              height: 40,
                              child: Text(data.selectedAnswer ?? ""))
                        ],
                      );
              },
            ),
          ),
          Center(
              child: Container(
            width: 150,
            color: Colors.blueAccent,
            child: TextButton(
              child: Text(
                'Reset Quiz',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                provider.resetQuiz();
                Navigator.pop(context);
                //Navigator.pushNamed(context, "/home");
              },
            ),
          ))
        ]));
  }
}
