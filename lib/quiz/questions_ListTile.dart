import 'package:flutter/material.dart';
import 'package:futurama/model/quiz_model.dart';
import 'package:provider/provider.dart';

import '../providers/quiz_provider.dart';

class QuestionListTile extends StatelessWidget {
  QuestionListTile({Key? key, this.queston}) : super(key: key);
  final QuizData? queston;

  @override
  Widget build(BuildContext context) {
    QuizProvider provider = Provider.of<QuizProvider>(context, listen: false);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0, top: 8.0),
            child: Text(
              "Questions",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            child: Text(
              queston!.question ?? "",
              style: TextStyle(fontSize: 16),
            ),
            width: 300,
          ),
          Wrap(
              children: queston!.possibleAnswers!
                  .map((e) => ListTile(
                        leading: Radio<String>(
                            value: e,
                            groupValue: provider.selectedAnswer ?? "",
                            onChanged: (value) {
                              provider.setAnswer(queston, value);
                            }),
                        title: Text(
                          e,
                          style: TextStyle(fontSize: 15),
                        ),
                      ))
                  .toList()),
          SizedBox(
            height: 40,
          ),
          Center(
              child: Container(
                  width: 100,
                  color: Colors.blueAccent,
                  child: TextButton(
                      onPressed: () {
                        if (provider.selectedAnswer!.isNotEmpty) {
                          provider.submitQuestion(
                              queston, provider.selectedAnswer ?? "");
                          if (provider.currentIndex ==
                              provider.quizData.length - 1) {
                            Navigator.pushNamed(context, "/results");
                          }
                        } else {}
                      },
                      child: Text("Next",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold)))))
        ],
      ),
    );
  }
}
