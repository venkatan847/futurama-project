import 'package:flutter/material.dart';
import 'package:futurama/providers/quiz_provider.dart';
import 'package:futurama/quiz/questions_ListTile.dart';
import 'package:provider/provider.dart';

class QuizScreen extends StatelessWidget {
  const QuizScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    QuizProvider provider = Provider.of<QuizProvider>(context);
    return Scaffold(
        backgroundColor: Colors.white,
        body: provider.isLoading
            ? Center(
                child: CircularProgressIndicator(
                  color: Colors.blue,
                ),
              )
            : loadQuestion(provider));
  }

  loadQuestion(QuizProvider provider) {
    final questionData = provider.quizData[provider.currentIndex];
    return Padding(
      padding: const EdgeInsets.only(left: 15.0, right: 15, top: 100),
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text("Total Questions : ${provider.quizData.length + 1}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Text("Score : ${provider.score}",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
          Center(
            child: Card(
              color: Colors.white,
              child: QuestionListTile(
                queston: questionData,
              ),
            ),
          ),
        ],
      )),
    );
  }
}
