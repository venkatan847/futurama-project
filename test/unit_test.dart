
import 'package:flutter_test/flutter_test.dart';
import 'package:futurama/appTheam/appstate_notifier.dart';
import 'package:futurama/providers/quiz_provider.dart';

void main() {
 setUpAll((){
 });
 group('',(){
  test('Unit Testing', (){
   AppStateNotifier appStateNotifier = AppStateNotifier(isDarkModeOn: true);
   bool value = appStateNotifier.isDarkModeOn;
   expect(value, false);
  });
  test('Unit Testing', (){
   AppStateNotifier appStateNotifier = AppStateNotifier();
   appStateNotifier.updateTheme(false);
   bool value = appStateNotifier.isDarkModeOn;
   expect(value, false);
  });
  test('Unit Testing', (){
   QuizProvider provider = QuizProvider();
   provider.currentIndex++;
   int index = provider.currentIndex;
   expect(index, 1);
  });
 });

}
